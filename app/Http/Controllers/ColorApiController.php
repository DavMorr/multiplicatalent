<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Color;
use App\Http\Resources\Color as ColorResource;
use App\Http\Resources\ColorCollection;

class ColorApiController extends Controller
{
    public function index(Request $request) 
    { 
        $pagination = $request->input('paginacion','') == null ? Color::paginate(6) : Color::paginate($request->paginacion);
        
        if ($request->input('tipo','') == null || $request->input('tipo','') == 'json') {
            return new ColorCollection($pagination);
        } else if  ($request->input('tipo','') == 'xml') {
            return response()->xml(new ColorCollection($pagination));
        } else {
            return new ColorCollection($pagination);
        }
    } 
  
    public function show(Color $color) 
    {
        $this->authorize('view', $color);

        return response()->json(new ColorResource($color), 200);
    } 
 
    public function store(Request $request) 
    {
        $this->authorize('create', Color::class);

        $validatedData = $request->validate([ 
            'Name' => 'required|string|max:25', 
            'Color' => 'required|string|max:25', 
            'Pantone' => 'required|string|max:50',
            'Year' => 'required|integer|digits:4||max:'.(date('Y')+1),
        ]);

        $color = Color::create($request->all());

        return response()->json($color, 201);
    } 
 
    public function update(Request $request, Color $color) 
    {
        $this->authorize('update', $color);

        $validatedData = $request->validate([ 
            'Name' => 'required|string|max:25', 
            'Color' => 'required|string|max:25', 
            'Pantone' => 'required|string|max:50',
            'Year' => 'required|integer|digits:4||max:'.(date('Y')+1),
        ]);

        $color->update($request->all());
 
        return response()->json($color, 200);
    } 
 
    public function delete(Request $request, Color $color) 
    {
        $this->authorize('delete', $color);

        $color->delete();
 
        return  response()->json(null, 204);;
    }
}
