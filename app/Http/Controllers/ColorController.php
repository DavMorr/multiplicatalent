<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Color;

class ColorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() 
    { 
        $data = Color::simplePaginate(9);
        return view('home', ['colores' => $data, 'rol' => auth()->user()->role]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if(auth()->user()->role != 'ROLE_USER') {
            return view('colores.create');
        }
        $data = Color::simplePaginate(9);
        return view('home', ['colores' => $data, 'rol' => auth()->user()->role]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'Name' => 'required|string|max:25', 
            'Color' => 'required|string|max:25', 
            'Pantone' => 'required|string|max:50',
            'Year' => 'required|integer|digits:4||max:'.(date('Y')+1),
        ]);

        Color::create($request->all());

        return redirect()->route('colores.index')
            ->with('success','Se ha agregado un color exitosamente');
    }

    /**
     * Display the specified resource.
     */
    public function show(Color $colore)
    {
        return view('colores.show',compact('colore'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Color $colore)
    {
        if(auth()->user()->role != 'ROLE_USER') {
            return view('colores.edit',compact('colore'));
        }
        $data = Color::simplePaginate(9);
        return view('home', ['colores' => $data, 'rol' => auth()->user()->role]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Color $colore)
    {
        $request->validate([ 
            'Name' => 'required|string|max:25', 
            'Color' => 'required|string|max:25', 
            'Pantone' => 'required|string|max:50',
            'Year' => 'required|integer|digits:4||max:'.(date('Y')+1),
        ]);

        $colore->update($request->all());

        return redirect()->route('colores.index')
            ->with('success','Registro actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Color $colore)
    {
        $colore->delete();

        return redirect()->route('colores.index')
            ->with('success','Registro eliminado exitosamente');
    }

}
