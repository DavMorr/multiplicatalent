<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Color extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [ 
            'ID' => $this->ID, 
            'Name' => $this->Name, 
            'Color' => $this->Color,
            'Pantone' => $this->Pantone,
            'Year' => $this->Year,
        ];
    }
}
