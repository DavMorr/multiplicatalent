@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header color-title row-double">
                    <div class="col-xs-12 col-sm-12 col-md-6" style="text-align: left;">
                        {{ __('Editar Color') }}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6" style="text-align: right;">
                        <form action="{{ route('colores.destroy',$colore->ID) }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Eliminar Color</button>
                        </form>
                    </div>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Se presentaron algunos problemas con tu solicitud.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                <form action="{{ route('colores.update',$colore->ID) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="card-body row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Nombre:</strong>
                                <input type="text" name="Name" value="{{ $colore->Name }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Color:</strong>
                                <input type="text" name="Color" value="{{ $colore->Color }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Pantone:</strong>
                                <input type="text" name="Pantone" value="{{ $colore->Pantone }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Year:</strong>
                                <input type="text" name="Year" value="{{ $colore->Year }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Actualizar Color</button>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection