@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header color-title">{{ __('Colores') }}</div>

                <div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="colores-container">
                    @foreach($colores as $color)
                    <!-- <a href="{{ route('colores.edit',$color->ID) }}"> -->
                    <div class="colores-item" style="background-color:{{$color->Color}}; @if($rol != 'ROLE_USER') cursor:pointer; @endif"  @if($rol != 'ROLE_USER') onclick="edit({{$color->ID}});"  @endif>
                        <div class="color-year">{{$color->Year}}</div>
                        <div class="color-name">{{$color->Name}}</div>
                        <div class="color-color">{{$color->Color}}</div>
                        <div class="color-pantone">{{$color->Pantone}}</div>
                    </div>
                    <!-- </a> -->
                    @endforeach
                    </div>

                    <div class="color-btn-paginate">
                    {{ $colores->links() }}
                    </div>

                    <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function edit(id_color) {
        console.log(id_color);
        var ruta = "{{ route('colores.edit','id_color')}}";
        console.log(ruta);
        ruta.replace('id_color',id_color);
        console.log(ruta.replace('id_color',id_color));
        window.location.href = ruta.replace('id_color',id_color);
    }
</script>
@endsection
